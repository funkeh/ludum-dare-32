return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.11.0",
  orientation = "orthogonal",
  width = 20,
  height = 12,
  tilewidth = 20,
  tileheight = 20,
  nextobjectid = 7,
  properties = {},
  tilesets = {
    {
      name = "bg8",
      firstgid = 1,
      tilewidth = 400,
      tileheight = 240,
      spacing = 0,
      margin = 0,
      image = "../gfx/bg8.png",
      imagewidth = 400,
      imageheight = 240,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "butterBathtub",
      firstgid = 2,
      tilewidth = 400,
      tileheight = 240,
      spacing = 0,
      margin = 0,
      image = "../gfx/butterBathtub.png",
      imagewidth = 400,
      imageheight = 240,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "butterColin",
      firstgid = 3,
      tilewidth = 400,
      tileheight = 240,
      spacing = 0,
      margin = 0,
      image = "../gfx/butterColin.png",
      imagewidth = 400,
      imageheight = 240,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "butterColinFace",
      firstgid = 4,
      tilewidth = 400,
      tileheight = 240,
      spacing = 0,
      margin = 0,
      image = "../gfx/butterColinFace.png",
      imagewidth = 400,
      imageheight = 240,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "butterWater",
      firstgid = 5,
      tilewidth = 400,
      tileheight = 240,
      spacing = 0,
      margin = 0,
      image = "../gfx/butterWater.png",
      imagewidth = 400,
      imageheight = 240,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "butterButter",
      firstgid = 6,
      tilewidth = 400,
      tileheight = 240,
      spacing = 0,
      margin = 0,
      image = "../gfx/butterButter.png",
      imagewidth = 400,
      imageheight = 240,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Tile Layer 1",
      x = 0,
      y = 0,
      width = 20,
      height = 12,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "objectgroup",
      name = "background",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 1,
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 240,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 1,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "bathtub",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 2,
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 240,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 2,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "colinBody",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 3,
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 240,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 3,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "colinFace",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 4,
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 240,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 4,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "water",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 5,
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 240,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 5,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "butter",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 6,
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 240,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 6,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
