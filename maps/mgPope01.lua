return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.11.0",
  orientation = "orthogonal",
  width = 20,
  height = 12,
  tilewidth = 20,
  tileheight = 20,
  nextobjectid = 12,
  properties = {},
  tilesets = {
    {
      name = "pope01",
      firstgid = 1,
      tilewidth = 400,
      tileheight = 240,
      spacing = 0,
      margin = 0,
      image = "../gfx/pope01.png",
      imagewidth = 400,
      imageheight = 240,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "pope01Enter",
      firstgid = 2,
      tilewidth = 400,
      tileheight = 240,
      spacing = 0,
      margin = 0,
      image = "../gfx/pope01Enter.png",
      imagewidth = 400,
      imageheight = 240,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "pope01Login",
      firstgid = 3,
      tilewidth = 400,
      tileheight = 240,
      spacing = 0,
      margin = 0,
      image = "../gfx/pope01Login.png",
      imagewidth = 400,
      imageheight = 240,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Tile Layer 1",
      x = 0,
      y = 0,
      width = 20,
      height = 12,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "objectgroup",
      name = "background",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 1,
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 240,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 1,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "login",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 3,
          name = "",
          type = "",
          shape = "rectangle",
          x = -20,
          y = 240,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 3,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "enter",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 2,
          name = "",
          type = "",
          shape = "rectangle",
          x = 20,
          y = 240,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 2,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "buttons",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 10,
          name = "mgPope02",
          type = "",
          shape = "rectangle",
          x = 132,
          y = 115.333,
          width = 44.3333,
          height = 17.3333,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 11,
          name = "mgPope03",
          type = "",
          shape = "rectangle",
          x = 224,
          y = 115,
          width = 41,
          height = 16.3333,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
