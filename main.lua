-- chikun :: 2015
-- Ludum Dare 32



--[[
    Here we will load the chikun specific libaries
  ]]
require "lib/bindings"      -- Very important that you read this file

-- Load particle functions
require "lib/iGrid"

-- Set default scaling
g.setDefaultFilter('nearest')

require "lib/cursors"
require "lib/maths"         -- Load maths functions
require "lib/misc"          -- Load miscellaneous functions

--[[
    The recursive loading functions pretty much convert / to . and
    make file extensions void.
    eg. gfx/player.png becomes gfx.player,
        sfx/enemies/die becomes sfx.enemies.die
  ]]
require "lib/load/fonts"    -- Load fonts into fnt table [not recursive]
require "lib/load/gfx"      -- Load png graphics from gfx folder to gfx table
require "lib/load/maps"     -- Load all maps from map folder to maps table
require "lib/load/snd"      -- Load ogg sounds from bgm and sfx to bgm and sfx
require "lib/load/states"   -- Load states from src/states to states table

require "lib/stateManager"  -- Load state manager
require "lib/mapTool"       -- Load mapTool :: don't try reading this
require "src/mg"


-- Performed at game startup
function love.load()


    -- Set current state to play state
    state.load(states.splash)

    gameSpeed = 1


end



-- Performed on game update
function love.update(dt)


    --[[
        Limit dt to 1/15 and induce lag if below 15 FPS.
        This is to save calculations.
      ]]
    dt = math.min(dt, 1/15)

    dt = dt * gameSpeed


    curs = {
        x = m.getX() / 2,
        y = m.getY() / 2,
        w = 1, h = 1
    }


    -- Update current state
    state.current:update(dt)


    if inMG then

        mini:update(dt)

    end

    disturbed = false

    lcurs = {
        x = curs.x,
        y = curs.y
    }
end



-- Performed on game draw
function love.draw()


    resetScaling()


    -- Set drawing colour to white [default]
    g.setColor(255, 255, 255)


    -- Draw current state
    state.current:draw()

    mini:draw()


    g.origin()


end


function love.mousemoved(x, y, dx, dy)

    disturbed = true

end


function love.mousepressed(x, y, mb)

    disturbed = true

end


function love.keypressed(key, isrepeat)

    disturbed = true

end



function resetScaling()


    g.origin()


    g.scale(2)


end


function checkCanvasBlank(canvas)

    local iData, iBreak = canvas:getImageData(), false

    for x = 0, canvas:getWidth() - 1 do

        for y = 0, canvas:getHeight() - 1 do

            local pixel = {iData:getPixel(x, y)}

            if pixel[4] ~= 0 then

                iBreak = true

            end

            if iBreak then break end

        end

        if iBreak then break end

    end

    return (not iBreak)

end


function checkCanvasBlankPercent(canvas)

    local iData, iBreak,iCount = canvas:getImageData(), false, 0

    for x = 0, canvas:getWidth() - 1 do

        for y = 0, canvas:getHeight() - 1 do

            local pixel = {iData:getPixel(x, y)}

            if pixel[4] == 0 then

                iCount = iCount + 1

            end
        end
    end

    return iCount / (canvas:getHeight() * canvas:getWidth())

end

