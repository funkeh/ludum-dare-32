-- chikun :: 2015
-- Minigame controller


-- In minigame?
inMG = false

-- Lives?
lives = 3

score = 1

mini = { }

currentMG = ""


mini.games = {
    baby = {
        title   = "YOU CAN SHAVE THE BABY",
        bgm     = bgm.baby,
        sfx     = sfx.shaveTheBaby,
        state   = states.minigames.baby,
        timer   = 12
    },
    pope = {
        title   = "DOWNLOAD THE POPE",
        bgm     = bgm.pope,
        sfx     = sfx.downloadThePope,
        state   = states.minigames.pope,
        timer   = 10
    },
    eggs = {
        title   = "YOU HAVE TO EAT THE EGGS",
        bgm     = bgm.eggs,
        sfx     = sfx.eatTheEggs,
        state   = states.minigames.eggs,
        timer   = 10
    },
    make = {
        title   = "MAKE ME BEAUTIFUL",
        bgm     = bgm.make,
        sfx     = sfx.makeMeBeautiful,
        state   = states.minigames.make,
        timer   = 15
    },
    pool = {
        title   = "STRADDLE THE POOL NOODLE",
        bgm     = bgm.pool,
        sfx     = sfx.poolNoodle,
        state   = states.minigames.pool,
        timer   = 5
    },
    lesbo = {
        title   = "LESBIANS",
        bgm     = bgm.lesbo,
        sfx     = sfx.lesbians,
        state   = states.minigames.lesbo,
        timer   = 10
    },
    bird = {
        title   = "DON'T SPOOK THE BIRD",
        bgm     = bgm.bird,
        sfx     = sfx.bird,
        state   = states.minigames.bird,
        timer   = 5
    },
    butter = {
        title   = "BUTTER UP",
        bgm     = bgm.patter,
        sfx     = sfx.butterUp,
        state   = states.minigames.colin,
        timer   = 10
    },
    tattoo = {
        title   = "GIVE HIM A TATTOO",
        bgm     = bgm.tattoo,
        sfx     = sfx.giveTattoo,
        state   = states.minigames.tattoo,
        timer   = 10
    },
    disguise = {
        title   = "CONCOCT A DISGUISE",
        bgm     = bgm.disguise,
        sfx     = sfx.concoctADisguise,
        state   = states.minigames.disguise,
        timer   = 10
    },
    pizza = {
        title   = "PIZZA PANTS",
        bgm     = bgm.pizza,
        sfx     = sfx.pizzaPants,
        state   = states.minigames.pizza,
        timer   = 12
    },
    gameOver = {
        title   = "",
        bgm     = bgm.bird,
        sfx     = sfx.nextLevel,
        state   = states.minigames.gameOver,
        timer   = 90000
    }
}


function mini:start(newMG)

    currentMG = newMG

    inMG = true

    mgPass = false

    mgTimer = mini.games[newMG].timer

end


function mini:update(dt)

    fadeOutTimer = math.max(fadeOutTimer - dt, 0)

    mgTimer = math.max(mgTimer - dt, 0)

    if mgTimer == 0 and nextMGName ~= "gameOver" then

        state.current = states.play

        states.play:updateCanvas()

        fadeDir = "in"

        inMG = false

        nextMG = nil

    end
end


function mini:draw()

    g.setFont(fnt.regular)

    if inMG and nextMGName ~= "gameOver" then

        g.setColor(0, 0, 0)

        g.print(math.ceil(mgTimer), 9, 201)

        g.setColor(255, 255, 255)

        g.print(math.ceil(mgTimer), 8, 200)

        drawTitle(255 * fadeOutTimer)

    elseif nextMG then

        drawTitle(255)

        fadeOutTimer = 1

    end

    if state.current ~= states.splash and state.current ~= states.main and
        state.current ~= states.credits and state.current ~= states.intro then

        g.setColor(255, 255, 255)

        for i=1, lives do

            g.draw(gfx.life, 8 + (i - 1) * 24, 8)

        end
    end
end


function drawTitle(alpha)

        g.setColor(0, 0, 0, alpha)

        g.printf(nextMG.title, 1, 48, 400, 'center')

        g.setColor(255, 255, 255, alpha)

        g.printf(nextMG.title, 0, 48, 400, 'center')

end
