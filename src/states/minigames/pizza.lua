-- chikun :: 2014
-- Bird minigame


-- Temporary state, removed at end of script
local pizzaState = { }


-- On state create
function pizzaState:create()


    won = false

    winTimer = 0

    pizzaCanvas = g.newCanvas(400, 240)

    g.setCanvas(pizzaCanvas)

        g.setColor(255, 255, 255)

        g.draw(gfx.pizzaOutlineWhite, 5, 5)

    g.setCanvas()

    map.setCurrent(maps.mgPizza)

    for key, layer in ipairs(map.current.layers) do

        if layer.name == "pizzaPants" then

            pants = layer.objects[1]

        elseif layer.name == "pizzaCrust" then

            crust = layer.objects[1]

        elseif layer.name == "pizzaOutline" then

            outline = layer.objects[1]

        end
    end
end


-- On state update
function pizzaState:update(dt)

    m.setCursor(cursor.pizza)

    if won then

        mgPass = true

        winTimer = winTimer + dt

        outline.col = {255, 255, 255, 255 - math.min(winTimer * 2, 1) * 255}

        pantsVal = math.clamp(0, (winTimer - 0.5) * 2, 1)

        pants.y = -240 * pantsVal
        crust.y = 240 * pantsVal

        if winTimer > 2 then

            mgTimer = 0

        end
    else

        if m.isDown('l') then

            g.setCanvas(pizzaCanvas)

                g.setColor(0, 0, 0, 0)

                g.setBlendMode('replace')

                g.setLineWidth(28)

                g.line(curs.x, curs.y, lcurs.x, lcurs.y)

            g.setCanvas()

            g.setLineWidth(1)

            g.setBlendMode('alpha')
        end

        if checkCanvasBlank(pizzaCanvas) then

            won = true
        end
    end

end


-- On state draw
function pizzaState:draw()

    g.setColor(255, 255, 255)

    if winTimer > 1 then
        gfxPizza = gfx.pizzaSuccess1
        if (winTimer * 8) % 2 > 1 then
            gfxPizza = gfx.pizzaSuccess2
        end
        g.draw(gfxPizza)
    else
        map.draw()

        g.draw(pizzaCanvas, 0, 0)
    end
end


-- On state kill
function pizzaState:kill()

end



-- Transfer data to state loading script
return pizzaState
