-- chikun :: 2014
-- Bird minigame


-- Temporary state, removed at end of script
local aphState = { }


-- On state create
function aphState:create()



    -- Load baby map
    map.setCurrent(maps.mgDisguise)

    lost = false

    won = false

    mPlaced = false

    monocle = {
        x = -64,
        y = 32 + math.random(176),
        alpha = 1
    }

    moustache = {
        x = -64,
        y = 32 + math.random(176),
        alpha = 1
    }

    timer = 0

    -- This is how we interpret map data
    for key, layer in ipairs(map.current.layers) do

        if layer.name == "aphrodite" then

            aphrodite = layer.objects[1]

        end
    end
end


-- On state update
function aphState:update(dt)

    if lost then

        aphrodite.x = aphrodite.x + dt * 64
        aphrodite.y = aphrodite.y + dt * 64

        monocle.alpha = math.max(monocle.alpha - dt, 0)

        if mPlaced then

            moustache.x = moustache.x + dt * 64
            moustache.y = moustache.y + dt * 64

        else

            moustache.alpha = math.max(moustache.alpha - dt, 0)

        end

        timer = timer + dt

    elseif won then

        timer = timer + dt

    else

        local yMove = 0


        if k.isDown('down') then

            yMove = 1
        end

        if k.isDown('up') then

            yMove = -1
        end


        if mPlaced then

            monocle.x = monocle.x + 128 * dt

            monocle.y = monocle.y + (160 * dt * yMove)

            if monocle.x >=269 then

                if math.abs(monocle.y - 87) < 6 then

                    monocle.x = 269

                    monocle.y = 87

                    won = true

                    mgPass = true

                else

                    lost = true

                end
            end

        else

            moustache.x = moustache.x + 128 * dt

            moustache.y = moustache.y + (160 * dt * yMove)

            if moustache.x >=244 then

                if math.abs(moustache.y - 126) < 6 then

                    moustache.x = 244

                    moustache.y = 126

                    mPlaced = true

                else

                    lost = true

                end
            end
        end
    end

    if timer > 1 then

        mgTimer = 0
    end
end


-- On state draw
function aphState:draw()

    g.setColor(255, 255, 255)

    if won then

        local loc = gfx.disguiseWin1

        if (timer * 8) % 2 > 1 then

            loc = gfx.disguiseWin2

        end

        g.draw(loc, 0, 0)

    else

        map.draw()

        g.setColor(255, 255, 255, moustache.alpha * 255)

        g.draw(gfx.disguiseMoustache, moustache.x - 45, moustache.y - 40)

        g.setColor(255, 255, 255, moustache.alpha * 255)

        g.draw(gfx.disguiseMonocle, monocle.x - 28, monocle.y - 24)
    end
end


-- On state kill
function aphState:kill()

end



-- Transfer data to state loading script
return aphState
