-- chikun :: 2014
-- Bird minigame


-- Temporary state, removed at end of script
local birdState = { }


-- On state create
function birdState:create()


    birdHappy = true

    birdTimer = 0

end


-- On state update
function birdState:update(dt)

    if disturbed then

        birdHappy = false

    end

    if not birdHappy then

        birdTimer = birdTimer + dt

        if birdTimer > 1 then

            mgTimer = 0

        end
    end

    mgPass = birdHappy

end


-- On state draw
function birdState:draw()

    g.setColor(255, 255, 255)

    if birdHappy then

        g.draw(gfx.bg7)

        g.draw(gfx.birdHappy)

    else

        g.draw(gfx.bg7scared)

        if (birdTimer * 4) % 2 > 1 then

            g.draw(gfx.birdScared2)

        else

            g.draw(gfx.birdScared)

        end
    end
end


-- On state kill
function birdState:kill()

end



-- Transfer data to state loading script
return birdState
