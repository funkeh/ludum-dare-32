-- chikun :: 2014
-- Pool minigame


-- Temporary state, removed at end of script
local tattooState = { }


-- On state create
function tattooState:create()


    -- Load pool map
    map.setCurrent(maps.mgTattoo)

    winTimer = 0

    -- This is how we interpret map data
    for key, layer in ipairs(map.current.layers) do

        if (layer.name == "tattooArm") then

            tattooArm = layer.objects[1]

        elseif (layer.name == "tattooArmDone") then

            tattooArmDone = layer.objects[1]

            tattooArmDone.col = { 255, 255, 255, 0 }

        elseif (layer.name == "tattooZone") then

            layer.objects[1].col = { 0, 0, 0, 255 }

        end
    end

    armCanvas = g.newCanvas(400, 240)

    g.setCanvas(armCanvas)

        g.draw(gfx.tattooZone)

    g.setCanvas()

    canvPerc = checkCanvasBlankPercent(armCanvas)
end


-- On state update
function tattooState:update(dt)

    m.setCursor(cursor.tattoo)

    local tatPercent = (checkCanvasBlankPercent(armCanvas) - canvPerc) / (1 - canvPerc)

    if tatPercent < 0.20 then


        if m.isDown('l') then

            g.setCanvas(armCanvas)

                g.setColor(0, 0, 0, 0)

                g.setBlendMode('replace')

                g.line(curs.x, curs.y, lcurs.x, lcurs.y)

            g.setCanvas()

            g.setBlendMode('alpha')

        end

    else

        mgPass = true

        tattooArm.col = { 255, 255, 255, 0 }

        tattooArmDone.col = { 255, 255, 255, 255 }

        winTimer = winTimer + dt

        if winTimer > 1 then

            mgTimer = 0

        end
    end
end


-- On state draw
function tattooState:draw()

    g.setColor(255, 255, 255)

    map.draw()

    g.draw(armCanvas)
end


-- On state kill
function tattooState:kill()

end



-- Transfer data to state loading script
return tattooState
