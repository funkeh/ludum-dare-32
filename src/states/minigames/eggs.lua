-- chikun :: 2014
-- Baby minigame


-- Temporary state, removed at end of script
local eggsState = { }


-- On state create
function eggsState:create()


    -- Load baby map
    map.setCurrent(maps.mgEggs)


    -- Timer for blushing
    eggs = { }


    mouth = {
        x = 287,
        y = 106
    }


    deadEggs = { }

    lost = false


    winTimer = 0

    bgCol = {220, 220, 220}


    -- This is how we interpret map data
    for key, layer in ipairs(map.current.layers) do

        if (layer.name == "eggs") then

            -- Set the eggs table
            for key, value in ipairs(layer.objects) do

                value.falling   = false
                value.vSpeed    = 0

                eggs[#eggs + 1] = value

            end

        elseif (layer.name == "woman") then

            woman = layer.objects[1]

        end
    end
end


-- On state update
function eggsState:update(dt)

    if not clicked and m.isDown('l') then

        -- Mouse cursor collision box
        for id = #eggs, 1, -1 do

            local egg = eggs[id]

            if math.overlap(egg, curs) and not lost then

                eggSelected = id

                break

            end
        end

    elseif clicked and not m.isDown('l') then

        if eggSelected then

            eggs[eggSelected].falling = true

            eggSelected = nil

        end
    end


    -- Falling eggs
    for id = #eggs, 1, -1 do

        local egg = eggs[id]

        if egg.falling then

            egg.vSpeed = egg.vSpeed + dt * 256

            egg.y = egg.y + egg.vSpeed * dt


            if egg.y > 240 then

                lost = true

                bgCol = {0, 0, 0}

            end

        elseif eggSelected == id then

            egg.x = curs.x - 18

            egg.y = curs.y - 23

            if math.dist(curs.x, curs.y, mouth.x, mouth.y) < 8 then

                egg.col = {255, 255, 255, 0}

                table.remove(eggs, id)

                deadEggs[#deadEggs + 1] = {
                    x = egg.x,
                    y = egg.y,
                    timer = 1
                }

                eggSelected = nil

            end
        end
    end


    for key, value in ipairs(deadEggs) do

        value.timer = value.timer - dt * 2

        if value.timer <=0 then

            table.remove(deadEggs, key)

        end
    end


    -- If no eggs left
    if #eggs == 0 then

        -- Player has won minigame
        mgPass = true

        bgCol = {100, 255, 200}

        winTimer = winTimer + dt

        woman.y = (1 - math.cos(math.rad(winTimer * 1200)))* 12

        if winTimer > 1 then

            mgTimer = 0

        end

    end

    clicked = m.isDown('l')

    if lost then

        woman.x = woman.x + 128 * dt

        if woman.x > 128 then

            mgTimer = 0

        end
    end
end


-- On state draw
function eggsState:draw()

    g.setColor(bgCol)

    g.rectangle("fill", 0, 0, 400, 240)

    g.setColor(255, 255, 255)

    map.draw()

    for key, value in ipairs(deadEggs) do

        g.setColor(255, 255, 255, value.timer * 255)

        g.draw(gfx.eggs, value.x, value.y)

    end
end


-- On state kill
function eggsState:kill()

    -- Kill eggs
    eggs        = nil
    eggSelected = nil

end


-- Transfer data to state loading script
return eggsState
