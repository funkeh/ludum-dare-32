-- chikun :: 2014
-- Lesbian minigame


-- Temporary state, removed at end of script
local lesboState = { }


-- On state create
function lesboState:create()


    -- Load lesbo map
    map.setCurrent(maps.mgLesbians)


    -- This is how we interpret map data
    for key, layer in ipairs(map.current.layers) do

        if (layer.name == "lesbianLeft") then

            leftbian = layer.objects[1]

        elseif (layer.name == "lesbianRight") then

            rightbian = layer.objects[1]


        elseif (layer.name == "lesbianChin") then

            chinbian = layer.objects[1]

        elseif (layer.name == "lesbianLove") then

            lovebian = layer.objects[1]

            lovebian.col = { 255, 255, 255, 0 }

        end
    end
end


-- On state update
function lesboState:update(dt)

    if not leftPressed and k.isDown('left') then

        rightbian.x = math.max(rightbian.x - 8, -44)

    end

    if not rightPressed and k.isDown('right') then

        leftbian.x = math.min(leftbian.x + 8, 24)

    end

    if leftbian.x == 24 and rightbian.x == -44 then

        mgPass = true

    end

    if mgPass then

        lovebian.col[4] = math.min(lovebian.col[4] + dt * 255, 255)

        if lovebian.col[4] == 255 then

            mgTimer = 0

        end
    end

    chinbian.x = rightbian.x

    leftPressed     = k.isDown('left')
    rightPressed    = k.isDown('right')

end


-- On state draw
function lesboState:draw()

    g.setColor(255, 255, 255)

    map.draw()
end


-- On state kill
function lesboState:kill()

end



-- Transfer data to state loading script
return lesboState
