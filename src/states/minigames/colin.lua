-- chikun :: 2014
-- Colin minigame


-- Temporary state, removed at end of script
local colinState = { }


-- On state create
function colinState:create()

    -- Load butter map
    map.setCurrent(maps.mgButter)


    butterCanvas = g.newCanvas(400, 240)

    g.setCanvas(butterCanvas)

        g.draw(gfx.butterSkinOverlay, 0, 0)

    g.setCanvas()

    winTimer = 0

    won = false

    -- This is how we interpret map data
    for key, layer in ipairs(map.current.layers) do

        if layer.name == "colinFace" then

            layer.objects[1].col = { 0, 0, 0, 0 }

        end
    end

    faceLoop = {
        gfx.butterColinFace,
        gfx.butterColinFace1,
        gfx.butterColinFace2,
        gfx.butterColinFace1
    }
end


-- On state update
function colinState:update(dt)

    m.setCursor(cursor.butter)

    g.setCanvas(butterCanvas)

        g.setColor(0, 0, 0, 0)

        g.setBlendMode('replace')

        g.setLineWidth(44)

        g.line(curs.x, curs.y, lcurs.x, lcurs.y)

        g.setLineWidth(1)

    g.setCanvas()

    g.setBlendMode('alpha')

    if not won then

        if checkCanvasBlank(butterCanvas) then

            won = true

            mgPass = true

        end
    end

    if won then

        winTimer = winTimer + dt

        if winTimer > 2 then

            mgTimer = 0

        end
    end
end


-- On state draw
function colinState:draw()

    g.setColor(255, 255, 255)

    map.draw()

    g.draw(butterCanvas, 0, 0)

    local scalVal = math.ceil(((winTimer % 1) - 0.7) / 0.1)

    scalVal = math.max(scalVal, 0)

    g.draw(faceLoop[scalVal + 1], 0, 0)
end


-- On state kill
function colinState:kill()

    butterCanvas = nil

end



-- Transfer data to state loading script
return colinState
