-- chikun :: 2014
-- Baby minigame


-- Temporary state, removed at end of script
local babyState = { }


-- On state create
function babyState:create()


    -- Load baby map
    map.setCurrent(maps.mgBaby)


    -- Timer for blushing
    blushTimer = 0


    -- This is how we interpret map data
    for key, layer in ipairs(map.current.layers) do

        if (layer.name == "important") then

            for key, object in ipairs(layer.objects) do

                if (object.name == "playerStart") then

                    -- Set the player's position
                    player.set(object.x, object.y,
                               object.w, object.h)

                end
            end

        elseif (layer.name == "cheeks") then

            -- Set the collisions table
            blush = layer.objects[1]

            blush.col = { 255, 255, 255, 0 }

        elseif (layer.name == "baby") then

            -- Set the collisions table
            baby = layer.objects[1]

        elseif (layer.name == "hair") then

            -- Set the collisions table
            hair = layer.objects

        end
    end
end


-- On state update
function babyState:update(dt)


    m.setCursor(cursor.razor)


    -- Mouse cursor collision box
    cursBox = {
        x = curs.x - 5,
        y = curs.y - 2,
        w = 10, h = 5
    }


    -- Allows you to shave the baby
    for key, folicle in ipairs(hair) do

        -- Checks hair + razor collision
        if math.overlap(folicle, cursBox) then

            -- Shaves the baby
            table.remove(hair, key)

        end
    end


    -- BLINK CONTROL CENTRE
    if #hair == 0 then

        mgPass = true

        baby.gid = 10

        blushTimer  = (blushTimer + dt *3)

        blush.col = { 255, 255, 255,
            math.clamp(0, blushTimer - 1, 1) * 255 }

        if blushTimer > 3 then

            mgTimer = 0

        end

    end
end


-- On state draw
function babyState:draw()

    g.setColor(255, 255, 255)

    map.draw()

end


-- On state kill
function babyState:kill()

end


-- Transfer data to state loading script
return babyState
