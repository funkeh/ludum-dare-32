-- chikun :: 2014
-- Makeup minigame


-- Temporary state, removed at end of script
local makeState = { }


-- On state create
function makeState:create()

    currentTool = "lipstick"

    winTimer = 0


    -- Load baby map
    map.setCurrent(maps.mgBeautiful)



    eyeCanvas   = g.newCanvas(400, 240)

    g.setCanvas(eyeCanvas)

        g.draw(gfx.beautifulEyelinerOverlay)

    g.setCanvas()

    cheekCanvas = g.newCanvas(400, 240)

    g.setCanvas(cheekCanvas)

        g.draw(gfx.beautifulMascaraOverlay)

    g.setCanvas()

    lipCanvas   = g.newCanvas(400, 240)

    g.setCanvas(lipCanvas)

        g.draw(gfx.beautifulLipstickOverlay)

    g.setCanvas()



    -- This is how we interpret map data
    for key, layer in ipairs(map.current.layers) do

        if (layer.name == "nose") then

            nose = layer.objects[1]

            nose.col = { 255, 255, 255, 0 }

        elseif (layer.name == "eyesOpen") then

            eyeOverlay = layer.objects[1]

            eyeOverlay.col = { 255, 255, 255, 0 }


        elseif (layer.name == "tools") then

            tools = layer.objects

        end
    end
end


-- On state update
function makeState:update(dt)



    for key, value in ipairs(tools) do

        if math.overlap(value, curs) and (m.isDown('l') and not clicked) then

            currentTool = value.name

        end

    end

    m.setCursor(cursor[currentTool])

    local can = eyeCanvas

    if currentTool == "lipstick" then

        can = lipCanvas

    elseif currentTool == "mascara" then

        can = cheekCanvas

    end

    g.setCanvas(can)

        g.setColor(0, 0, 0, 0)

        g.setBlendMode('replace')

        g.circle('fill', curs.x, curs.y, 12)

    g.setCanvas()

    g.setBlendMode('alpha')

    clicked = m.isDown('l')

    if checkCanvasBlank(eyeCanvas) and checkCanvasBlank(lipCanvas) and
        checkCanvasBlank(cheekCanvas) then

        mgPass = true

        winTimer = winTimer + dt

        if winTimer > 1 then

            mgTimer = 0

        end

    end

end


-- On state draw
function makeState:draw()

    g.setColor(255, 255, 255)

    map.draw()

    g.draw(eyeCanvas, 0, 0)

    g.draw(cheekCanvas, 0, 0)

    g.draw(lipCanvas, 0, 0)

    g.draw(gfx.beautifulNose, 0, 0)

    if mgPass then

        g.draw(gfx.beautifulEyesOpen, 0, 0)

    end
end


-- On state kill
function makeState:kill()

    eyeCanvas = nil

    lipCanvas = nil

    cheekCanvas = nil

end



-- Transfer data to state loading script
return makeState
