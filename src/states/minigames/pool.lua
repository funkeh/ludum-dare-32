-- chikun :: 2014
-- Pool minigame


-- Temporary state, removed at end of script
local poolState = { }


-- On state create
function poolState:create()


    -- Load pool map
    map.setCurrent(maps.mgPool2)



    -- This is how we interpret map data
    for key, layer in ipairs(map.current.layers) do

        if (layer.name == "man") then

            man = layer.objects[1]

            man.col = {0,0,0,0}

            newMan = {
                x = man.x + man.w / 2,
                y = man.y + man.h / 2,
                scale = 1
            }

        elseif (layer.name == "noodle") then

            noodle = layer.objects[1]

            nDir = 1

        end
    end

    falling = false

    closeup = false
end


-- On state update
function poolState:update(dt)


    if not closeup then

        noodle.x = noodle.x + nDir * dt * 96

        if noodle.x > 214 or noodle.x < 10 or math.random(500) == 1 then

            nDir = -nDir

        end

        if not falling then

            newMan.y = newMan.y - 192 * dt

            if newMan.y < -64 then

                falling = true

                newMan.x = noodle.x + noodle.w / 2

                newMan.scale = 0.6

            end
        else

            local xMove = 0

            if k.isDown('left') then

                xMove = -1
            end

            if k.isDown('right') then

                xMove = 1
            end

            newMan.x = newMan.x + 112 * dt * xMove

            newMan.y = newMan.y + 128 * dt

            if newMan.y > 160 then

                passed = (newMan.x > noodle.x and newMan.x < noodle.x + noodle.w)

                hideNoodle = not (newMan.x > noodle.x - 32 and
                    newMan.x < noodle.x + noodle.w + 32)

                mgPass = passed

                closeup = true

                map.setCurrent(maps.mgPool)


                local activeName = "poolLegFront"

                if passed then

                    activeName = "poolLegBack"

                end

                -- This is how we interpret map data
                for key, layer in ipairs(map.current.layers) do

                    if layer.name == activeName then

                        activeLeg = layer.objects[1]

                    elseif layer.name == "poolMan" then

                        man = layer.objects[1]

                    elseif layer.name == "noodle" and hideNoodle then

                        layer.objects[1].col = {0,0,0,0}
                    end
                end
            end
        end



    else

        activeLeg.y = activeLeg.y + 256 * dt

        if passed and activeLeg.y > 306 - activeLeg.h then

            activeLeg.y = 306 - activeLeg.h

        end

        man.y = activeLeg.y
    end
end


-- On state draw
function poolState:draw()

    g.setColor(255, 255, 255)

    map.draw()

    if not closeup then

        g.draw(gfx.poolManSmall, newMan.x, newMan.y, 0, newMan.scale,
            newMan.scale, man.w / 2, man.h / 2)

    end
end


-- On state kill
function poolState:kill()

end



-- Transfer data to state loading script
return poolState
