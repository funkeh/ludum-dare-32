-- chikun :: 2014
-- Bird minigame


-- Temporary state, removed at end of script
local goState = { }


-- On state create
function goState:create()

    fadeIn = 0

    fadeOut = 0

    anim = 0

end


-- On state update
function goState:update(dt)

    fadeIn = math.min(fadeIn + dt * 2, 1)

    anim = (anim + dt) % 1

    if fadeOut > 0 then

        fadeOut = fadeOut + dt

        if fadeOut >= 1 then

            state.change(states.play)

        end

    elseif fadeIn == 1 then

        if m.isDown('l') then

            if curs.x < 200 then

                fadeOut = fadeOut + dt

            else

                state.change(states.credits)

            end
        end
    end
end


-- On state draw
function goState:draw()

    g.setColor(255, 255, 255)

    local gra = gfx.gameOver

    if anim > 0.5 then

        gra = gfx.gameOver2

    end

    g.draw(gra, 0, 0)

    g.setFont(fnt.regular)

    g.printf("You hacked into " .. score .. " interfaces!", 0, 84, 400, 'center')

    g.setColor(255, 255, 255, fadeIn * 255)

    g.draw(gfx.gameOverCredits)

    g.draw(gfx.gameOverReplay)

    g.setColor(0, 0, 0, fadeOut * 255)

    g.rectangle("fill", 0, 0, 400, 240)
end


-- On state kill
function goState:kill()

end



-- Transfer data to state loading script
return goState
