-- chikun :: 2014
-- Pool minigame


-- Temporary state, removed at end of script
local tattooState = { }


-- On state create
function tattooState:create()

    -- Load pool map
    map.setCurrent(maps.mgPope01)

    for key, layer in ipairs(map.current.layers) do

        if layer.name == "buttons" then

            buttons = layer.objects
        end
    end

    timerPope = 0
end


-- On state update
function tattooState:update(dt)

    if mgPass then

        timerPope = timerPope + dt

        if timerPope > 1 then

            mgTimer = 0

        end
    end

    for key, value in ipairs(buttons) do

        if not clicked and m.isDown('l') and math.overlap(curs, value) then

            if value.name == "mgPope07" then

                mgPass = true

            end

            map.setCurrent(maps[value.name])

            for key, layer in ipairs(map.current.layers) do

                if layer.name == "buttons" then

                    buttons = layer.objects
                end
            end
        end
    end

    clicked = m.isDown('l')
end


-- On state draw
function tattooState:draw()

    g.setColor(255, 255, 255)

    map.draw()
end


-- On state kill
function tattooState:kill()

end



-- Transfer data to state loading script
return tattooState
