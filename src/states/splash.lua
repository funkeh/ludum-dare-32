-- chikun :: 2014
-- Splash state


-- Temporary state, removed at end of script
local splashState = { }


-- On state create
function splashState:create()


    -- Set background colour to black
    g.setBackgroundColor(0, 0, 0)

    -- Set off timer
    timer = 0

    -- Has sound played
    soundPlayed = false

    --
    splCanvas = g.newCanvas(176, 48)

    g.origin()

    g.setCanvas(splCanvas)

        -- Set colour with alpha
        g.setColor(255, 255, 255)

        -- Set font
        g.setFont(fnt.splash)

        -- Draw logo with formatting
        g.printf("chikun", 88, 24 - (fnt.splash:getHeight() / 2), 0, 'center')

    g.setCanvas()

    resetScaling()

    splGrid = iGrid.new(splCanvas, 4)


end


-- On state update
function splashState:update(dt)


    -- Increase timer
    timer = timer + dt

    -- When one second passes, change state...
    if (#splGrid == 0) then

        -- ...to the next state
        state.change(states.main)

    -- If over half a second and sound hasn't played...
    elseif (timer >= 0.5 and not soundPlayed) then

        -- ...then play it!
        sfx.startup:play()

        iGrid.applyEffect(splGrid, pfx.vertAsh, 400)

        -- And set variable
        soundPlayed = true

    end

    iGrid.update(splGrid, dt)


end


-- On state draw
function splashState:draw()

    -- Set colour with alpha
    g.setColor(255, 255, 255, math.min(timer * 2, 1) * 255)

    iGrid.draw(splGrid, splCanvas, 112, 96)

end


-- On state kill
function splashState:kill()


    -- Kill all variables
    timer = nil
    soundPlayed = nil


end


-- Transfer data to state loading script
return splashState
