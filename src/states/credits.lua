-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local creditsState = { }


-- On state create
function creditsState:create()

    sfx.credits:play()

end


-- On state update
function creditsState:update(dt)

    if not sfx.credits:isPlaying() then

        state.change(states.splash)

    end

end


-- On state draw
function creditsState:draw()

    g.setColor(255, 255, 255)

    local tell = sfx.credits:tell("seconds")

    if tell < 6.55 then

        g.draw(gfx.credits01)

    elseif tell < 13.1 then

        g.draw(gfx.credits02)

    elseif tell < 19.65 then

        g.draw(gfx.credits03)

    elseif tell < 26.2 then

        g.draw(gfx.credits04)

    else

        g.draw(gfx.creditsThanks)

    end

end


-- On state kill
function creditsState:kill()

end


-- Transfer data to state loading script
return creditsState
