-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local newState = { }


-- On state create
function newState:create()

    g.setBackgroundColor(0, 0, 0)

    fade = 0

    fadeDir = 1

    step = 0

    bgm.theme:setVolume(fade)

    bgm.theme:play()

end


-- On state update
function newState:update(dt)

    fade = math.clamp(0, fade + dt * 2 * fadeDir, 1)


    if fadeDir == 1 then
        bgm.theme:setVolume(fade)
    end

    if fade == 1 then

        step = (step + dt) % 1

    else

        step = 0

    end

    if (m.isDown('l') or k.isDown(' ') or k.isDown('return')) and fade == 1 then

        fadeDir = -1

    end

    if fade == 0 then

        state.change(states.intro)

    end
end


-- On state draw
function newState:draw()

    g.setColor(255, 255, 255, fade * 255)

    g.draw(gfx.bg0, 0, 0)

    if (step > 0.5) then

        g.draw(gfx.startGame, 0, 0)

    end
end


-- On state kill
function newState:kill()

end


-- Transfer data to state loading script
return newState
