-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local newState = { }


-- On state create
function newState:create()

end


-- On state update
function newState:update(dt)

end


-- On state draw
function newState:draw()

end


-- On state kill
function newState:kill()

end


-- Transfer data to state loading script
return newState
