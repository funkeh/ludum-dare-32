-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local newState = { }


-- On state create
function newState:create()

    clicked = m.isDown('l')

    step = 1

end


-- On state update
function newState:update(dt)

    if not clicked and m.isDown('l') then

        step = step + 1

        if step == 9 then

            state.change(states.play)

        end
    end

    clicked = m.isDown('l')

end


-- On state draw
function newState:draw()

    g.draw(gfx["intro" .. step])

end


-- On state kill
function newState:kill()

    bgm.theme:stop()

end


-- Transfer data to state loading script
return newState
