-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local playState = { }


-- On state create
function playState:create()

    g.setBackgroundColor(255, 255, 255)

    mgCanvas = g.newCanvas(400, 240)

    g.setCanvas(mgCanvas)

        g.setColor(0, 0, 0)

        g.rectangle("fill", 0, 0, 400, 240)

    g.setCanvas()

    fadeDir = "level"

    fade = 0

    lives = 3

    score = 1

    olScore = 1

    genList()

    gNumber = #mgList

    mainGFX = gfx.comp

    bgm.play:play()
end


-- On state update
function playState:update(dt)

    m.setCursor(cursor.hand)

    if fadeDir == "out" then

        fade = math.min(fade + dt * 2, 1)

        nextMG.bgm:setVolume(fade)

        bgm.play:setVolume(1 - fade)

        if fade == 1 then

            state.current = nextMG.state

            mini:start(nextMGName)

        end
    elseif fadeDir == "in" then

        fade = math.max(fade - dt * 2, 0)

        mini.games[currentMG].bgm:setVolume(fade)

        bgm.play:setVolume(1 - fade)

        if fade == 0 then

            mini.games[currentMG].bgm:stop()

            fadeDir = "complete"

            mini.games[currentMG].state:kill()

            completeTimer = 5

            g.setCanvas(mgCanvas)

                if mgPass then

                    g.draw(gfx.passed1, 0, 0)

                    mainGFX = gfx.compWin1

                    gameSpeed = gameSpeed + 0.05

                    olScore = olScore + 1

                    sfx.succeed:play()

                else

                    g.draw(gfx.failed1, 0, 0)

                    mainGFX = gfx.compLose1

                    lives = lives - 1

                    gameSpeed = 1

                    olScore = 1

                    sfx.fail:play()

                end

            g.setCanvas()

            score = score + 1
        end

    elseif fadeDir == "complete" then

        local oldTimer = completeTimer

        completeTimer = math.max(completeTimer - dt * 2, 0)

        if math.ceil(oldTimer) > math.ceil(completeTimer) then

            g.setCanvas(mgCanvas)

                if mgPass then

                    if math.ceil(completeTimer % 2) == 2 then

                        mainGFX = gfx.compWin2

                        g.draw(gfx.passed2, 0, 0)

                    else

                        mainGFX = gfx.compWin1

                        g.draw(gfx.passed1, 0, 0)

                    end

                else

                    if math.ceil(completeTimer % 2) == 2 then

                        mainGFX = gfx.compLose2

                        g.draw(gfx.failed2, 0, 0)

                    else

                        mainGFX = gfx.compLose1

                        g.draw(gfx.failed1, 0, 0)

                    end

                end

            g.setCanvas()

        end

        if completeTimer == 0 then

            if lives == 0 then

                states.play:startMG("gameOver")

            else

                fadeDir = "level"

                fade = 0

            end

        end

    elseif fadeDir == "level" then

        mainGFX = gfx.comp

        if not sfx.nextLevel:isPlaying() then

            sfx.nextLevel:play()

        end

        fade = math.min(fade + dt, 1)

        g.setCanvas(mgCanvas)

            g.setColor(0, 0, 0)

            g.rectangle("fill", 0, 0, 400, 240)

            g.setColor(255, 255, 255)

            g.setFont(fnt.score)

            g.printf(score, 0, 40, 400, 'center')

        g.setCanvas()

        if fade == 1 then

            key = math.random(1, #mgList)

            states.play:startMG(mgList[key])

            table.remove(mgList, key)

            if #mgList == 0 then

                genList()

            end
        end
    end
end



-- Start fade out
function playState:startMG(newMG)

    mini.games[newMG].sfx:play()

    mini.games[newMG].bgm:setVolume(0)

    local gamePitch = math.floor(olScore / gNumber) * 0.2

    mini.games[newMG].bgm:setPitch(1 + gamePitch)

    mini.games[newMG].bgm:play()

    nextMG = mini.games[newMG]

    nextMGName = newMG

    nextMG.state:create()

    states.play:updateCanvas()

    fadeDir = "out"

    fade = 0

end


-- Update minigame canvas
function playState:updateCanvas()

    g.origin()

    mgCanvas:clear()

    g.setCanvas(mgCanvas)

        nextMG.state:draw()

    g.setCanvas()

end


-- On state draw
function playState:draw()

    g.draw(mainGFX, 0, 0)

    local fadeVal = 1

    if fadeDir == "out" or fadeDir == "in" or fadeDir == "complete" then

        fadeVal = 1 - fade

    end

    g.draw(mgCanvas, 218 * fadeVal, 86 * fadeVal, 0, (400 -289 * fadeVal) / 400)

end


-- On state kill
function playState:kill()

    mgCanvas = nil

end


function genList()

    mgList = { }

    for key, value in pairs(mini.games) do

        if key ~= "gameOver" then

            mgList[#mgList+1] = key
        end
    end
end


-- Transfer data to state loading script
return playState
