-- chikun :: 2015
-- Player script



player = {}



-- Called when map loads
player.set = function(x, y, w, h)


    -- Set player position
    player.x = x ; player.y = y
    player.w = w ; player.h = h

    -- Set player image
    player.image = gfx.player

    -- For player movement
    player.gravity      = 2048
    player.ySpeed       = 0
    player.ySpeedMax    = 768

    -- Set player score
    player.score = 0


end



-- Called using update phase
player.update = function(dt)


    -- Figure out which direction the player will move horizontally
    local xMove = 0
    if (k.isDown('left'))  then
        xMove = -1
    end
    if (k.isDown('right')) then
        xMove = xMove + 1
    end

    -- Actually move the player horizontal
    player.x = player.x + 256 * dt * xMove


    -- Check horizontal movement
    player.checkHorizontal(dt)


    -- Increase falling speed
    player.ySpeed = player.ySpeed + player.gravity * dt

    -- Restrict falling speed
    player.ySpeed = math.min(player.ySpeed, player.ySpeedMax)

    -- Actually make player fall
    player.y = player.y + player.ySpeed * dt


    -- Check vertical movement
    player.checkVertical(dt)


    -- Check coin collisions
    for key, coin in ipairs(coins) do

        -- If colliding with coin...
        if (math.overlap(coin, player)) then

            -- ...destroy coin
            table.remove(coins, key)

            -- Play sound effect
            sfx.coin:play()

            sfx.coin:setPitch(0.9 + 0.2 * math.random())

            -- Increase score
            player.score = player.score + 1

            -- If collected all coins
            if (#coins == 0) then

                -- Go to win state
               state.change(states.win)

            end
        end
    end


end



-- Called using draw phase
player.draw = function()


    -- Draw player
    g.draw(player.image, player.x, player.y)


end



-- Check vertical movement
player.checkVertical = function(dt)


    -- Can the player jump?
    local canJump = false

    -- Check if top half of player colliding
    topHalf = {
        x = player.x,
        y = player.y,
        w = player.w,
        h = player.h / 2
    }

    -- While colliding...
    while (math.overlapTable(collisions, topHalf)) do

        -- Move box out of collision
        topHalf.y = math.floor(topHalf.y + 1)

        -- Set player's ySpeed to 0
        player.ySpeed = 0

    end

    -- Move player to determined y position
    player.y = topHalf.y


    -- Check if bottom half of player colliding
    bottomHalf = {
        x = player.x,
        y = player.y + player.h / 2,
        w = player.w,
        h = player.h / 2
    }

    -- While colliding...
    while (math.overlapTable(collisions, bottomHalf)) do

        -- Move box out of collision
        bottomHalf.y = math.ceil(bottomHalf.y - 1)

        -- Set player's ySpeed to 0
        player.ySpeed = 0

        -- Allow player to jump
        canJump = true

    end

    -- Move player to determined y position
    player.y = bottomHalf.y - player.h / 2

    -- Make player jump if holding spacebar and can
    if (canJump and k.isDown(' ')) then

        -- Up they go
        player.ySpeed = -768

    end


end



-- Check horizontal movement
player.checkHorizontal = function(dt)


    -- Check if left half of player colliding
    leftHalf = {
        x = player.x,
        y = player.y,
        w = player.w / 2,
        h = player.h
    }

    -- While colliding...
    while (math.overlapTable(collisions, leftHalf)) do

        -- Move box out of collision
        leftHalf.x = math.floor(leftHalf.x + 1)

    end

    -- Move player to determined x position
    player.x = leftHalf.x

    -- Check if right half of player colliding
    rightHalf = {
        x = player.x + player.w / 2,
        y = player.y,
        w = player.w / 2,
        h = player.h
    }

    -- While colliding...
    while (math.overlapTable(collisions, rightHalf)) do

        -- Move box out of collision
        rightHalf.x = math.floor(rightHalf.x - 1)

    end

    -- Move player to determined x position
    player.x = rightHalf.x - player.w / 2


end
