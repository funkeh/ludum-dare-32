-- chikun :: 2014
-- Loads all maps from the /maps folder


-- Load map functions
require "lib/mapTool"


-- Recursively checks a folder for maps
-- and adds any found to a maps table
function checkFolder(dir, tab)

    local tab = ( tab or { } )

    -- Get a table of files / subdirs from dir
    local items = f.getDirectoryItems(dir)

    for key, val in ipairs(items) do

        if f.isFile(dir .. "/" .. val) then
            -- If item is a file, then load it into tab

            -- Remove ".lua" extension on file
            local name = val:gsub(".lua", "")

            local dirTest = dir .. "/"
            dirTest = dirTest:gsub("maps/", "")

            -- Load map into table
            tab[name] = map.load(dirTest .. name)

            -- If supplementary functions exist
            if f.isFile("src/mapFunctions/" .. dirTest .. name .. ".lua") then

                -- Load map functions
                tab[name].__FUNCTIONS__ = require("src/mapFunctions/" .. dirTest .. name)

            end

        else
            -- Else, run checkFolder on subdir

            -- Add new table onto tab
            tab[val] = { }

            -- Run checkFolder in this subdir
            checkFolder(dir .. "/" .. val, tab[val])

        end

    end

    return tab

end


-- Load the maps folder into the maps table
maps = checkFolder("maps")

-- Kills checkFolder
checkFolder = nil
