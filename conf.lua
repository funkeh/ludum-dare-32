-- chikun :: 2015
-- The configuration file for our tutorial


--[[
    The function which runs at the start of the game, and
    allows us to modify the game window
  ]]
function love.conf(game)

    game.window.width   = 800
    game.window.height  = 480
    game.window.title   = "You Can Shave the Baby"
    game.window.vsync   = false
    game.window.icon    = "gfx/icon.png"

    --[[
        On Windows, will attach a debug console to the game.
        Set to false before distribution.
      ]]
    game.console = false

end
