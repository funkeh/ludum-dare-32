-- chikun :: 2015
-- Spin particle effect


local spin = { }


function spin:apply(grid, speed)

    for key, value in ipairs(grid) do

        local tx, ty = (value.x + value.w / 2), (value.y + value.h / 2)

        value.dir   = math.deg(math.atan2(ty - grid.ox, tx - grid.oy))
        value.dist  = ((tx - grid.ox) ^ 2 + (ty - grid.oy) ^ 2) ^ 0.5

    end

    grid.timer  = 0
    grid.speed  = speed

end


function spin:update(grid, dt)

    -- Increase grid timer
    local speed = grid.speed / 64

    for key, value in ipairs(grid) do

        value.y = value.y - 64 * dt

        --[[
        value.dist = value.dist * (1 + 0.2 * speed)

        value.dir = (value.dir + (120 * dt * speed)) % 360

        local dir = math.rad(value.dir)

        value.x = math.cos(dir) * value.dist
        value.y = math.sin(dir) * value.dist

        value.alpha = value.alpha - 1 * dt * speed

        if value.alpha <= 0 then

            table.remove(grid, key)

        end]]

    end

end


return spin
